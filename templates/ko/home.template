<!--VAR:[__layout|open]-->
<!--TITLE:[Windows 프로그램을 리눅스, BSD, Solaris, macOS에서 실행하기]-->
<!--META_KEYWORDS:[windows, linux, macintosh, solaris, freebsd]-->
<!--META_DESCRIPTION:[Windows 프로그램을 다른 운영체제에서 실행하는 오픈 소스 소프트웨어입니다.]-->
<!--META_OG:[type|website]-->

<div class="whq-page-container margin-bottom-md">

    <div class="row">
        <div class="col-md-7">

            <h1 class="title margin-bottom-sm">Wine은 무엇인가요?</h1>

            <p>Wine(원래 "Wine Is Not an Emulator"의 약어)은 리눅스, macOS, BSD와 같은 POSIX 호환 운영체제에서 Windows 프로그램을 실행할 수 있는 호환성 레이어입니다. 가상 머신이나 에뮬레이터와 같이 내부 Windows 로직을 시뮬레이션하는 대신 Wine은 Windows API 호출을 POSIX 시스템 호출로 즉시 대체합니다. 다른 방식과 다르게 성능이나 메모리 문제가 적으며, Windows 프로그램을 데스크톱에 깔끔하게 통합할 수 있습니다.</p>

            <br>

            <h2 class="title margin-bottom-sm">최신 릴리스</h2>
            <div class="row">
                <div class="col-sm-4 col-md-3">안정 버전:</div>
                <div class="col-sm-8">
                    <b><a href="{$root}/announce/{$config_stable_release}">Wine&nbsp;{$config_stable_release}</a></b>
                    <span class="small">
                      (<a href="//source.winehq.org/git/wine.git?a=shortlog;h=refs/tags/wine-{$config_stable_release}">변경 기록 요약</a>)
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-3">개발 버전:</div>
                <div class="col-sm-8">
                    <b><a href="{$root}/announce/{$config_master_release}">Wine&nbsp;{$config_master_release}</a></b>
                    <span class="small">
                        (<a href="//source.winehq.org/git/wine.git?a=shortlog;h=refs/tags/wine-{$config_master_release}">변경 기록 요약</a>)
                    </span>
                </div>
            </div>

            <br>

            <div id="carouselScreenshots" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/steam.jpg" alt="슬라이드 1">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/outlook2010.jpg" alt="슬라이드 2">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/eveonline.jpg" alt="슬라이드 3">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/photoshopcc2015.jpg" alt="슬라이드 4">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/falloutnv.jpg" alt="슬라이드 5">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/reason.jpg" alt="슬라이드 6">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/skyrim_specialed.jpg" alt="슬라이드 7">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/goggalaxy.jpg" alt="슬라이드 8">
                    </div>
                    <div class="item active">
                        <img src="https://dl.winehq.org/share/images/screenshots/wizard101.jpg" alt="슬라이드 9">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/matlab.jpg" alt="슬라이드 10">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/sims3.jpg" alt="슬라이드 11">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/metatrader5.jpg" alt="슬라이드 12">
                    </div>
                </div>
                <a class="left carousel-control" href="#carouselScreenshots" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">이전</span>
                </a>
                <a class="right carousel-control" href="#carouselScreenshots" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">다음</span>
                </a>
            </div>

        </div>
        <div class="col-md-5">

            <div class="winehq_menu">

                <a class="winehq_menu_item info" href="{$root}/about">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-info" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">정보</span>
                    <span class="subtitle">Wine 프로젝트에 대해서 알아 봅니다.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item dl" href="https://wiki.winehq.org/Download">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-download" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">다운로드</span>
                    <span class="subtitle">최신 Wine 버전을 설치합니다.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item appdb" href="https://appdb.winehq.org">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-database" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">프로그램 데이터베이스</span>
                    <span class="subtitle">사용하고자 하는 프로그램이 Wine에서 작동하나요?</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item help" href="{$root}/help">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-question" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">지원</span>
                    <span class="subtitle">Wine 사용에 대한 도움을 얻을 수 있습니다.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item devel" href="{$root}/getinvolved">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-users" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">참여하기</span>
                    <span class="subtitle">Wine 기능 개선과 개발에 참여하세요.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item donate" href="{$root}/donate">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-donate" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">기부하기</span>
                    <span class="subtitle">Wine 프로젝트에 기부해 주세요.</span>
                </a>
                <div class="clear"></div>

            </div>

        </div>
    </div>
</div>

<div class="whq-page-container margin-top-md">

    <h2 class="title"><a href="http://wiki.winehq.org/Organising_WineConf">WineConf 호스트 모집</a></h2>

    <p>올해 중으로 WineConf를 개최할 장소를 모집하고 있습니다. 다음 사항을 포함하여 제안해 주십시오.</p>

    <ul>
        <li>제안하는 날짜</li>
        <li>제안하는 도시</li>
        <li>가능성 있는 장소와 여행 정보</li>
    </ul>

    <p>행사를 계획하고 예약하는 데 시간이 걸리기 때문에 최대한 빠르게 장소를 구하고 있습니다. 2월 말까지 개최지를 정하기를 희망하지만 최종 결정은 Wine 위원회에서 진행될 예정입니다. 질문, 의견, 댓글 등은 <a href="{$root}/forums">WineConf 메일링 리스트</a>에 게시할 수 있습니다. 호스팅 가이드의 초안을 위키에 작성하기 시작했습니다 [1]. 컨퍼런스를 개최하는 데 필요한 자원을 알아 보려면 페이지를 읽어 보십시오.</p>

    <p>1. <a href="https://wiki.winehq.org/Organising_WineConf">https://wiki.winehq.org/Organising_WineConf</a></p>

    <br>

    <h2 class="title"><a href="{$root}/news">뉴스와 업데이트</a></h2>

    <!--EXEC:[news?n=3]-->

    <p><a href="{$root}/news" class="btn btn-default"><span class="glyphicon glyphicon-chevron-right"></span> 뉴스 더 보기...</a></p>

</div>


