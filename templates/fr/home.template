<!--VAR:[__layout|open]-->
<!--TITLE:[Exécuter des applications Windows sous Linux, BSD, Solaris et macOS]-->
<!--META_KEYWORDS:[windows, linux, macintosh, solaris, freebsd]-->
<!--META_DESCRIPTION:[Logiciel Open Source pour l'exécution d'applications Windows sur d'autres systèmes d'exploitation.]-->
<!--META_OG:[type|website]-->

<div class="row">
    <div class="col-sm-7">

        <div class="whq-page-container margin-bottom-md">
            <h3 class="title">Dernières versions</h3>
            <div class="row">
                <div class="col-sm-4 col-md-3">Stable :</div>
                <div class="col-sm-8">
                    <b><a href="{$root}/announce/{$config_stable_release}">Wine&nbsp;{$config_stable_release}</a></b>
                    <span class="small">
                      (<a href="//source.winehq.org/git/wine.git?a=shortlog;h=refs/tags/wine-{$config_stable_release}">changements</a>)
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-3">Développement :</div>
                <div class="col-sm-8">
                    <b><a href="{$root}/announce/{$config_master_release}">Wine&nbsp;{$config_master_release}</a></b>
                    <span class="small">
                        (<a href="//source.winehq.org/git/wine.git?a=shortlog;h=refs/tags/wine-{$config_master_release}">changements</a>)
                    </span>
                </div>
            </div>
        </div>

        <div class="whq-page-container">

            <div class="winehq_menu nopadding margin-bottom-lg">
                <a class="winehq_menu_item info nomargin" href="{$root}/about">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-info" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">À propos</span>
                    <span class="subtitle">Apprenez-en plus sur le projet Wine.</span>
                </a>
                <div class="clear"></div>
            </div>

            <p>Wine (à l'origine un acronyme pour « Wine Is Not an Emulator ») est une couche de compatibilité capable d'exécuter des applications Windows sur divers systèmes d'exploitation conformes à POSIX comme Linux, macOS et BSD. Plutôt que de simuler la logique interne de Windows comme une machine virtuelle ou un émulateur, Wine traduit les appels de l'API Windows en appels POSIX à la volée, éliminant les pénalités mémoire et de performance d'autre méthodes et vous permettant d'intégrer proprement les applications Windows à votre bureau.</p>

        </div>

    </div>
    <div class="col-sm-5 winehq_menu_wrap fill-height">

        <div class="whq-page-container winehq_menu fill-height">

                <a class="winehq_menu_item dl" href="https://wiki.winehq.org/Download">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-download" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Téléchargement</span>
                    <span class="subtitle">Installez la dernière version de Wine.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item help" href="{$root}/help">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-question" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Support</span>
                    <span class="subtitle">Obtenez de l'aide sur Wine.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item devel" href="{$root}/getinvolved">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-users" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Développement</span>
                    <span class="subtitle">Devenez un développeur Wine.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item donate" href="{$root}/donate">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-donate" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Dons</span>
                    <span class="subtitle">Faites un don au projet Wine.</span>
                </a>
                <div class="clear"></div>

        </div>

    </div>
</div>

<div class="whq-page-container margin-top-md">

    <h1 class="title"><a href="{$root}/news">Nouvelles et mises à jour</a></h1>

    <!--EXEC:[news?n=3]-->

    <p><a href="{$root}/news" class="btn btn-default"><span class="glyphicon glyphicon-chevron-right"></span> plus de nouvelles...</a></p>

</div>

