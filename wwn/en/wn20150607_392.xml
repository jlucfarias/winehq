<?xml version="1.0" ?>
<kc>
<title>Wine Traffic</title>

<author contact="http://www.dawncrow.de/wine/">Andr&#233; Hentschel</author>
<issue num="392" date="06/07/2015" />
<intro><![CDATA[ <p>This is the 392nd issue of the World Wine News publication.
Its main goal is to inform you of what's going on around Wine. Wine is an open source implementation of the Windows API on top of X and Unix.  Think of it as a Windows compatibility layer.  Wine does not require Microsoft Windows, as it is a completely alternative implementation consisting of 100% Microsoft-free code, but it can optionally use native system DLLs if they are available.   You can find more info at <a href="http://www.winehq.org">www.winehq.org</a></p> ]]></intro>
<stats posts="140" size="102" contrib="36" multiples="17" lastweek="11">

<person posts="6" size="8" who="austinenglish at gmail.com (Austin English)" />
<person posts="6" size="4" who="piotr.caban at gmail.com (Piotr Caban)" />
<person posts="5" size="7" who="bunglehead at gmail.com (Nikolay Sivov)" />
<person posts="5" size="5" who="hverbeet at gmail.com (Henri Verbeet)" />
<person posts="4" size="4" who="matteo.mystral at gmail.com (Matteo Bruni)" />
<person posts="3" size="5" who="jwhite at codeweavers.com (Jeremy White)" />
<person posts="3" size="4" who="sebastian at fds-team.de (Sebastian Lackner)" />
<person posts="3" size="3" who="julliard at winehq.org (Alexandre Julliard)" />
<person posts="3" size="2" who="aeikum at codeweavers.com (Andrew Eikum)" />
<person posts="2" size="5" who="scottritchie at ubuntu.com (Scott Ritchie)" />

</stats>

<section
	title="WineConf 2015"
	subject="WineConf"
	archive="https://wiki.winehq.org/WineConf2015"
	posts="1"
>
<![CDATA[
<p>
Instead of visiting FOSDEM a third time we decided to do our own conference again this year.
WineConf will be held in Vienna in September and we have a very interesting preliminary agenda:
</p>
<ul>
<li>Keynote presentation – Alexandre Julliard</li>
<li>Wine Staging Status Update – TBD</li>
<li>Wine Stable Considered Harmful - Michael Stefaniuc / Open discussion</li>
<li>Wine USB Overview, Current status, Next steps - Ulrich Czekalla / Open discussion</li>
<li>Regressions - Open discussion</li>
<li>Tricking Alexandre into accepting your patches - Michael Stefaniuc</li>
<li>Wine Tests Hackathon - Jeremy White</li>
<li>Wine Staging Patch Signoff Hackathon - Sebastian Lackner</li>
<li>hid, hidclass and joystick unification - Aric Stewart</li>
<li>The other architectures - André Hentschel</li>
<li>Full Stack On Wine - Qian Hong</li>
</ul>
<p>
Possibly we'll also have:
</p>
<ul>
<li>Wine's desktop and system integration - Damjan Jovanovic</li>
<li>Wine and Valgrind - Austin English</li>
</ul>
]]>
</section>

<section
	title="Google Summer of Code 2015"
	subject="GSoC"
	archive="https://www.google-melange.com/gsoc/homepage/google/gsoc2015"
	posts="1"
>
<![CDATA[
<p>
I haven't wrote about GSoC 2015 yet, but the Wine Project already was
<a href="http://www.phoronix.com/scan.php?page=news_item&px=GSoC-2015-Interesting-Work">on the news</a> with it.
We have four projects which are already in the coding period:
</p>

<p><b>Direct3DRM - Implement rendering backend for D3DRM</b> -
<a href="https://www.google-melange.com/gsoc/project/details/google/gsoc2015/jam/5750085036015616">Project</a>
<a href="//www.winehq.org/pipermail/wine-devel/2015-May/107608.html">by Aaryaman Vasishta</a></p>
<blockquote>
<p>Thank you for inviting me to this opportunity! I will try my best to keep it short, though it might be a bit long for some. There's a TL;DR at the end, though. :)</p>
<p>A bit about myself. My name is Aaryaman Vasishta and I'm currently studying in my third year of Computer Engineering in Pune Institute of Computer Technology, India. My interests lie in game programming and computer graphics.</p>
<p>My project focuses on implementing the rendering backend for the D3DRM API [1].</p>
<p>D3DRM (Direct3D Retained Mode) is basically a scene graph API running on top of Direct3D's Immediate Mode API. You can say it's more like a rendering engine API which encapsulates Immediate Mode functionality in order to make it easier for programmers to develop 3D scenes using it, making it a possible competitor to OpenGL at the time.</p>
<p>At the moment wine's implementation of this API is mostly full of stubs, and there's quite a bit of work left before something can be drawn on the screen. My role here mainly focuses on implementing object Creation/Initialization functions for some of the main interfaces, mainly devices, textures and viewports, all of which are COM based. If time permits, I will also work on implementing some frequently used frames and lighting functions.</p>
<p>The API is quite old (it has been removed since DX 8 SDK, and the dll doesn't come included with vista onwards) but there are a few popular games that used it. Namely, Lego Rock Raiders and Steel Beasts, and applications as well, like FMS (Flying Model Simulator). So there is some merit in working on this. Implementing these functions will help accelerate further development of this API to get some long-awaited apps to run on wine (I can see quite a few threads on google of people trying to get FMS running, and a couple for LRR too, so there is some demand for it). As an added bonus, I also get to interact with wine's ddraw implementation for this one, which could potentially help ddraw's implementation via possible bug detection/fixes and implementing any ddraw functionality that d3drm requires.</p>
<p>TL;DR: I'm implementing a main chunk of a graphics API called Direct3D Retained Mode, which is based on Direct3D Immediate Mode. The API is mostly a stub in wine and this project should help get things going.</p>
<p>[1] - <a href="https://www.google-melange.com/gsoc/project/details/google/gsoc2015/jam/5750085036015616">https://www.google-melange.com/gsoc/project/details/google/gsoc2015/jam/5750085036015616</a></p>
</blockquote>

<p><b>Implement the Visual C++ iostream library</b> -
<a href="https://www.google-melange.com/gsoc/project/details/google/gsoc2015/sulley/5685265389584384">Project</a>
<a href="//www.winehq.org/pipermail/wine-devel/2015-May/107619.html">by Iván Matellanes</a></p>
<blockquote>
<p>I'm looking forward to contributing to Wine.</p>
<p>My project consists on implementing part of the legacy Visual C++ iostream runtime, which was shipped with Visual Studio versions up to 6.0 and is currently a stub. I'll work on as many functions as time permits, and one of the key points is to reuse code from the modern Visual C++ runtime library that is already implemented.</p>
<p>Some old applications and games (like MS Reader and Tron 2.0) would benefit from this, as they would run with the built-in library. A quick search on Bugzilla for 'msvcirt' shows several bugs related to unimplemented functions.</p>
</blockquote>

<p><b>Implementing functions from tr2 namespace</b> -
<a href="https://www.google-melange.com/gsoc/project/details/google/gsoc2015/yonghaohu/5741031244955648">Project</a>
<a href="//www.winehq.org/pipermail/wine-devel/2015-May/107628.html">by YongHao Hu</a></p>
<blockquote>
<p>My project focuses on implementing all the functions from tr2 namespace, which was included in the <filesystem> header and being proposed for standardization.</p>
<p>Though there are many methods to implement the functions like _File_size and _Equivalent etc, the hard part is finding the most appropriate one.</p>
<p>New applications like MSVC12[1] would benefit from this.</p>
<p>[1]: <a href="//bugs.winehq.org/show_bug.cgi?id=35774">bugs.winehq.org/show_bug.cgi?id=35774</a></p>
</blockquote>

<p><b>Improve mshtml.dll</b> -
<a href="https://www.google-melange.com/gsoc/project/details/google/gsoc2015/zhenbo/5709068098338816">Project</a>
<a href="//www.winehq.org/pipermail/wine-devel/2015-May/107609.html">by Zhenbo Li</a></p>
<blockquote>
<p>I'm glad to working on Wine GSoC this year. My project's focus is
IHMLTXMLHttpRequest. Many websites would use hacks to determine
whether the browser was IE6.0 or IE 7+. As XMLHttpRequest object
identifier was shipped in IE 7.0[0], the web developers would use
ActiveX to access IXMLHttpRequest object. Wine IE implements some new
features, so it is common that Wine IE is treated as a IE 7+
browser(like Firebug Lite[1])</p>

<p>Mozilla has implemented nsIXMLHttpRequest[2], and my approach is to
call the wine-gecko functions from wine code.
I can't tell how many applications' status on appdb will change from
"garbage" to "silver/gold", but IMHO, implementing XMLHttpRequest is
necessary to make wine IE more usable.</p>

<p>[0]: <a href="http://en.wikipedia.org/wiki/XMLHttpRequest">http://en.wikipedia.org/wiki/XMLHttpRequest</a></p>
<p>[1]: <a href="https://getfirebug.com/firebuglite">https://getfirebug.com/firebuglite</a></p>
<p>[2]: <a href="https://developer.mozilla.org/en-US/docs/nsIXMLHttpRequest">https://developer.mozilla.org/en-US/docs/nsIXMLHttpRequest</a></p>
</blockquote>
]]>
</section>

<section
	title="Git Success Story"
	subject=""
	archive="https://www.linux.com/news/featured-blogs/200-libby-clark/822789-git-success-stories-and-tips-from-wine-maintainer-alexandre-julliard/"
	posts="1"
>
<![CDATA[
<p>
Alexandre Julliard was
<a href="https://www.linux.com/news/featured-blogs/200-libby-clark/822789-git-success-stories-and-tips-from-wine-maintainer-alexandre-julliard/">
interviewed by Linux.com</a> and it's an interesting read!
</p>
<blockquote>
<p>
I can no longer imagine doing software development without it.
</p>
</blockquote>
]]>
</section>

<section
	title="Social Media"
	subject="Social Media"
	archive="https://www.facebook.com/pages/winehqorg/1634558060090404"
	posts="0"
>
<![CDATA[
<p>
Tom Wickline finally set up a
<a href="https://www.facebook.com/pages/winehqorg/1634558060090404">Facebook</a> page for WineHQ!
Make sure to subscribe to it to receive interesting and Wine related posts.
</p>
]]>
</section>

<section
	title="Migrating away from Sourceforge"
	subject=""
	archive="//www.winehq.org/pipermail/wine-devel/2015-June/107722.html"
	posts="14"
>
<![CDATA[
<p>
Austin writes:
</p>
<blockquote>
<p>
Given
SourceForge's recent activities [1] with taking over open source
projects for their own gain, I propose that we migrate away from
SourceForge. The main uses for SourceForge is for mirroring tarballs,
wine-gecko/wine-mono installers, and the wine-gecko git repository.
</p>
<p>
[1] <a href="http://www.extremetech.com/computing/206687-sourceforge-accused-of-hijacking-gimp-photo-editor-company-claims-project-was-abandoned">
http://www.extremetech.com/computing/206687-sourceforge-accused-of-hijacking-gimp-photo-editor-company-claims-project-was-abandoned</a>
</p>
</blockquote>
<p>
So far there was mostly (or only) positive feedback to that suggestion and planning seems to be in progress.
</p>
]]>
</section>

<section
    title="Weekly AppDB/Bugzilla Status Changes"
    subject="AppDB/Bugzilla"
    archive="//appdb.winehq.org"
    posts="0"
>
<![CDATA[
<topic>AppDB / Bugzilla</topic>
<center><b>Bugzilla Changes:</b></center>
<p><center>
<table border="1" bordercolor="#222222" cellspacing="0" cellpadding="3">
  <tr><td align="center">
        <b>Category</b>
  </td><td>
         <b>Total Bugs Last Issue</b>
  </td><td>
        <b>Total Bugs This Issue</b>
  </td><td>
        <b>Net Change</b>
  </td></tr>  <tr>
    <td align="center">
     UNCONFIRMED
    </td>
    <td align="center">
     2993
    </td>
    <td align="center">
     2897
    </td>
    <td align="center">
      -96
    </td>
  </tr>  <tr>
    <td align="center">
     NEW
    </td>
    <td align="center">
     3472
    </td>
    <td align="center">
     3473
    </td>
    <td align="center">
      +1
    </td>
  </tr>  <tr>
    <td align="center">
     ASSIGNED
    </td>
    <td align="center">
     18
    </td>
    <td align="center">
     17
    </td>
    <td align="center">
      -1
    </td>
  </tr>  <tr>
    <td align="center">
     REOPENED
    </td>
    <td align="center">
     111
    </td>
    <td align="center">
     111
    </td>
    <td align="center">
      0
    </td>
  </tr>  <tr>
    <td align="center">
     RESOLVED
    </td>
    <td align="center">
     219
    </td>
    <td align="center">
     262
    </td>
    <td align="center">
      +43
    </td>
  </tr>  <tr>
    <td align="center">
     CLOSED
    </td>
    <td align="center">
     31794
    </td>
    <td align="center">
     31936
    </td>
    <td align="center">
      +142
    </td>
  </tr>   <tr><td align="center">
      TOTAL OPEN
   </td><td align="center">
      6594
   </td><td align="center">
      6498
   </td><td align="center">
      -96
   </td></tr>
   <tr><td align="center">
       TOTAL
   </td><td align="center">
      38607
   </td><td align="center">
      38696
   </td><td align="center">
      +89
   </td></tr>
</table>
</center></p>
<br /><br />
<center><b>AppDB Application Status Changes</b></center>
<p><i>*Disclaimer: These lists of changes are automatically  generated by information entered into the AppDB.
These results are subject to the opinions of the users submitting application reviews.
The Wine community does not guarantee that even though an application may be upgraded to 'Gold' or 'Platinum' in this list, that you
will have the same experience and would provide a similar rating.</i></p>
<div align="center">
   <b><u>Updates by App Maintainers</u></b><br /><br />
    <table width="80%" border="1" bordercolor="#222222" cellspacing="0" cellpadding="3">
      <tr>
        <td><b>Application</b></td>
        <td width="140"><b>Old Status/Version</b></td>
        <td width="140"><b>New Status/Version</b></td>
        <td width="20" align="center"><b>Change</b></td>
      </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=27919">Matlab R2010b</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.29)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.39)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=27325">Risen 2 - Dark Waters Steam</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.43)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31851">SuperPower 2 Steam</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.42)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.44)
             </td><td align="center">
                <div style="color: #000000;">+4</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=28103">Dead Island Riptide 1.4.0 (via Steam)</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.28)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.39)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=27183">DomDomSoft Manga Downloader 5</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.30)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=28142">FarCry 3 Blood Dragon Steam</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.42)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.43)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32212">Hyperdimension Neptunia Re;Birth 2 1.x</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.42)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32207">Weblica 3.7.3</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.43)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=15638">CD Wave 1.98</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.1.22)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td colspan="3">
                Total Change
             </td><td align="center">
               <div style="color: #000000;">+12</div>
             </td>
           </tr>
        </table>  <br />   <b><u> Updates by the Public </u></b> <br /><br />
   <table width="80%" border="1" bordercolor="#222222" cellspacing="0" cellpadding="3">
      <tr>
        <td><b>Application</b></td>
        <td width="140"><b>Old Status/Version</b></td>
        <td width="140"><b>New Status/Version</b></td>
        <td width="20"><b>Change</b></td>
      </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31491">Evernote 5.8.x</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6.2)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.43)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=21068">Spore 1.0</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.4.1)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=11315">Carriers At War (2007) 1.02.2</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (0.9.59)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=296">Command &amp; Conquer: Tiberian Sun 1.0-2.03</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.6.1)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31419">Gas Guzzlers Extreme 1.0.4</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.6.2)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.43)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=573">Homeworld 1.05</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.32)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.38)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32213">Lifeless Planet 1.4</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.39)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=25473">Multi Theft Auto: San Andreas 1.3</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.4.1)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.43)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=26175">Origin Latest Release</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.4.1)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32205">PAYDAY 2 1.34.2 - Alesso Heist</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.43)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=21984">Super Crate Box 1.02</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.6.2)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=30292">The Witcher 2.0.0.12 GOG</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.17)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=30403">Wolfenstein: The New Order 1.0</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.33)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.38)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=26406">Dragon Age: Origins Origin Downloader install.</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.4.1)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.43)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=9965">3Galactic Civilizations II: Dread Lords Twilight o...</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.2.3)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.43)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31794">League of Legends 5.x</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.36)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.43)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=6648">Rollercoaster Tycoon 3 Platinum</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.5.11)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=22972">Rome: Total War Rome TW Gold Edition</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.4)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.43)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=28849">Skullgirls Steam</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.11)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.43)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=2952">Tzar: The Burden of the Crown 1.x</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.3.19)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.44)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31201">ArchiCAD 18</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.30)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.43)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=19719">CPU-Z 1.x</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.38)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.43)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=29162">Grim Dawn Alpha Early Access</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.34)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.42)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=3921">MechCommander 2 1.x CD</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.1.16)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=30497">Pac-Man Championship Edition DX+ 1.0.4.1</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.20)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.43)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=10282">Prince of Persia: The Sands of Time 1.81</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.1.43)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.43)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31190">SketchUp 2015</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.39)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.43)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=22577">Adobe Reader 10.x</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.4.1)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.6.2)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=27093">Adobe Reader 11.x</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.6.2)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=26932">Deadlight 1.0</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.38)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.43)
             </td><td align="center">
                <div style="color: #990000;">-4</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31525">EasyWorship 6 6</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.33)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.38)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=20540">Prince of Persia: Warrior Within 1.x</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.5.4)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.43)
             </td><td align="center">
                <div style="color: #990000;">-4</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=20882">StarCraft II Current</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.39)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.44)
             </td><td align="center">
                <div style="color: #990000;">-4</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32222">Submachine 1 HD 1.1</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.6.1)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.6.2)
             </td><td align="center">
                <div style="color: #990000;">-4</div>
             </td>
           </tr>           <tr>
             <td colspan="3">
                Total Change
             </td><td align="center">
               <div style="color: #000000;">+1</div>
             </td>
           </tr>
        </table></div>
]]>
</section></kc>
