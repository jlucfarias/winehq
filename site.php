<?php

/*
  WineHQ
  by Jeremy Newman <jnewman@codeweavers.com>
*/

// load modules and defines
$file_root = realpath(dirname(__FILE__));
require("{$file_root}/include/incl.php");

// default page body
$PAGE_body = "";

// default page title
$PAGE_title = $config->site_name;

// default page template
$PAGE_template = "content";

// Fix the PHP_SELF global var
$_SERVER['PHP_SELF'] = preg_replace('/(site\.php\/|site\.php$)/', '', $_SERVER['PHP_SELF'], 1);
if (!preg_match('/\/$/', $_SERVER['PHP_SELF']))
    $_SERVER['PHP_SELF'] = $_SERVER['PHP_SELF']."/";

// store the original PHP_SELF for redirects, as the stop page code mangles it
$_SERVER['PHP_SELF_FULL'] = $_SERVER['PHP_SELF'];

// load the path for the page
if (isset($_SERVER['PATH_INFO']))
{
    // set the Current Page
    $dirs = preg_split('/\//', $_SERVER['PATH_INFO'], -1, PREG_SPLIT_NO_EMPTY);
    if (count($dirs) > 1)
    {
        $good_dirs = array();
        foreach ($dirs as $dir)
        {
            // current page as string
            array_push($good_dirs, $dir);
            $temp_page = join('/', $good_dirs);

            // if this page is defined as a stop page, anything after it is not a template but params
            if (in_array($temp_page, $config->stop_pages))
            {
                // get the params from the path
                $page_params = str_replace($temp_page, '', $_SERVER['PATH_INFO']);

                // cleanup the page params
                $page_params = preg_replace('/(^\/\/|^\/|\/$)/', '', $page_params);
                $page_params = $html->clean_input($page_params);
                if ($page_params)
                    define("PAGE_PARAMS", $page_params);

                // cleanup the PHP_SELF var
                $_SERVER['PHP_SELF'] = str_replace($page_params, '', $_SERVER['PHP_SELF']);
                $_SERVER['PHP_SELF'] = preg_replace('/\/\/$/', '/', $_SERVER['PHP_SELF']);

                // don't continue the loop, this is the end page
                unset($page_params);
                break;
            }

            // store the page
            unset($temp_page);
        }

        // cleanup and define page
        $page = join('/',$good_dirs);
        if (preg_match('/\/$/', $page))
            $page = preg_replace('/\/$/', '', $page);
        unset($c);
    }
    else
    {
        $page = $dirs[0];
    }

    // clean page path
    $page = $html->clean_input($page, true);

    // template PAGE path verification
    if (preg_match('%^global/%', $page))
    {
        // block direct access to all other global templates
        debug("global", "GLOBAL template access denined!");
        $html->in404 = 1;
        define("PAGE", '404');
    }
    else if (preg_match('/[a-z0-9_-]/i', $page))
    {
        // good path
        define("PAGE", $page);
    }
    else
    {
        // unknown
        define("PAGE", '404');
    }
    unset($page, $dirs, $good_dirs);
}
else
{
    define("PAGE", 'home');
}

// homepage old URL redirects
if (defined("PAGE") and PAGE == "home")
{
    // fix old WWN issue URLS
    if (isset($_GET['issue']) and intval($_GET['issue']) > 0)
    {
        $html->redirect("{$config->base_url}wwn/{$_GET['issue']}");
        exit();
    }

    // fix old interview URLS
    if (isset($_GET['interview']) and intval($_GET['interview']) > 0)
    {
        $html->redirect("{$config->base_url}interview/{$_GET['interview']}");
        exit();
    }

    // fix old announce URLS
    if (isset($_GET['announce']) and (intval($_GET['announce']) > 0 or $_GET['announce'] == 'latest'))
    {
        $html->redirect("{$config->base_url}announce/{$_GET['announce']}");
        exit();
    }

    // fix old news URLS
    if (isset($_GET['news']) and (intval($_GET['news']) > 0))
    {
        $html->redirect("{$config->base_url}news/{$_GET['news']}");
        exit();
    }
}

// online mode switch
if (!empty($config->offline))
{
    /*
       OFFLINE
    */
    $html->_error_mode = 1;
    $PAGE_body = $html->template("local", 'global/website_offline');
}
else
{
    /*
       ONLINE
    */

    // load our actual page
    $PAGE_body = $html->template("local", PAGE);
}

// use template title if specified
if (!empty($html->page_title))
    if ($html->force_title)
        $PAGE_title = "{$html->page_title}";
    else
        $PAGE_title = "{$PAGE_title} - {$html->page_title}";

// set meta tags
$PAGE_meta_keywords = "";
$PAGE_meta_description = "";
if (!empty($html->meta_keywords))
    $PAGE_meta_keywords = $html->meta("keywords", $html->meta_keywords);
if (!empty($html->meta_description))
    $PAGE_meta_description = $html->meta("description", $html->meta_description);

// open graph tags
$PAGE_meta_og = "";
if (empty($html->in403) and empty($html->in404) and isset($html->meta_og) and is_array($html->meta_og))
{
    // defaults and fallbacks
    if (empty($html->meta_og['title']) and !empty($PAGE_body_title))
        $html->meta_og['title'] =& $PAGE_body_title;
    if (empty($html->mega_og['site_name']))
        $html->meta_og['site_name'] = $config->site_name;
    if (empty($html->meta_og['image']))
        $html->meta_og['image'] = "{$html->base_url()}/images/";
    if (empty($html->meta_og['description']) and !empty($html->meta_description))
        $html->meta_og['description'] = $html->meta_description;
    if (empty($html->meta_og['type']))
        $html->meta_og['type'] = 'article';

    // loop and build meta_og string
    foreach ($html->meta_og as $og => $og_data)
    {
        if (is_array($og_data))
        {
            foreach ($og_data as $in_data)
            {
                if ($PAGE_meta_og)
                    $PAGE_meta_og .= "    ";
                $PAGE_meta_og .= "<meta property=\"og:{$og}\" content=\"{$in_data}\">\n";
            }
        }
        else
        {
            if ($PAGE_meta_og)
                $PAGE_meta_og .= "    ";
            $PAGE_meta_og .= "<meta property=\"og:{$og}\" content=\"{$og_data}\">\n";
        }
    }
}

// rss link
$PAGE_rss = "";
if (!empty($html->rss_link))
    $PAGE_rss = '<link rel="alternate" type="application/rss+xml" title="'.$PAGE_title.'" href="'.$html->rss_link.'">';

// rel canonical
$PAGE_relc = "";
if (!empty($html->_rel_canonical) and $html->_rel_canonical != "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}")
    $PAGE_relc = '<link rel="canonical" href="'.$html->_rel_canonical.'">';

// debugging output
debug("global", "Ending Debug Log");
debug("global", "Script Execution time: ".round(microtime_float() - $script_start_time, 5)." seconds");
$PAGE_debug = "";
if (in_array($html->_view_mode, array("default","nonav")) and ($config->web_debug or ($login->id and $login->web_debug)))
{
    $PAGE_debug = $html->template("local", "global/layout/debug_log", array("debug_log" => $html->encode($GLOBALS['debug_log'])));
}

// page body class
$PAGE_body_class = "";
if (in_array($html->lang, array("he")))
    $PAGE_body_class .= " rtl";

// vars for all web templates
$PAGE_vars = array(
                  'page_title'       => &$PAGE_title,
                  'page_name'        => (preg_replace('%\/%', '-', PAGE)),
                  'meta_keywords'    => &$PAGE_meta_keywords,
                  'meta_description' => &$PAGE_meta_description,
                  'meta_og'          => &$PAGE_meta_og,
                  'rel_canonical'    => &$PAGE_relc,
                  'css_links'        => $html->get_header_links('css'),
                  'js_links'         => $html->get_header_links('js'),
                  'js_preload'       => $html->get_header_links('js_pre'),
                  'js_postload'      => $html->get_header_links('js_post'),
                  'rss_link'         => &$PAGE_rss,
                  'page_body'        => &$PAGE_body,
                  'page_body_class'  => $PAGE_body_class,
                  'copyright_year'   => date("Y", time()),
                  'debug_log'        => &$PAGE_debug,
                  'langCur'          => $html->trans_val("lang", "lang", $html->lang),
                  'langChange'       => $html->trans_val("lang", "change", $html->lang)
                 );

// HTTP status headers
switch (true)
{
    // 403 - access denied
    case ($html->in403):
        header("HTTP/1.1 403 Forbidden");
        break;

    // 404 - page not found
    case ($html->in404):
        header("HTTP/1.1 404 Not Found");
        break;

    // 500 - server error
    case ($html->_error_mode):
        header("HTTP/1.1 500 Internal Server Error");
        break;
}

// display page based on view mode
switch ($html->_view_mode)
{
    // plain text view
    case "text":
        $html->http_header("text/plain");
        echo $PAGE_body;
        break;

    // xml view
    case "xml":
        $html->http_header("text/xml");
        echo $PAGE_body;
        break;

    // json view
    case "json":
        $html->http_header("text/javascript");
        echo $PAGE_body;
        break;

    // print view
    case "print":
        $html->http_header("text/html");
        echo $html->template("local", "global/layout/{$PAGE_template}_print", $PAGE_vars, 1);
        break;

    // ajax -- no html header/body tags
    case "ajax":
        $html->http_header("text/html");
        echo $html->template("local", "global/layout/{$PAGE_template}_ajax", $PAGE_vars);
        break;

    // regular view
    default:
        $html->http_header("text/html");
        $PAGE_layout = $html->template_var('__layout');
        switch ($PAGE_layout)
        {
            // no predefined
            case "open":
                break;
            // default layout
            case "":
                $PAGE_vars['page_body'] = "<div class=\"whq-page-container\">\n{$PAGE_vars['page_body']}\n</div>\n";
                break;
            // load layout from template
            default:
                $PAGE_vars['page_body'] = $html->template("local", "global/layout/body_{$PAGE_layout}",
                                                          array("page_body" => $PAGE_vars['page_body']));
        }
        echo $html->template("local", "global/layout/{$PAGE_template}", $PAGE_vars);
}

// done
?>
